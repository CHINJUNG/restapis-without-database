const express = require('express');
const app = express();

const port = process.env.PORT || 3000;

let tempArr = [];
let i = 1;

app.use(express.json());

app.get('/', (req, res) => {
    res.status(200).send(tempArr);
})

app.get('/:id', (req, res) => {
    const id = req.params.id;
    const result = tempArr.filter(el => el.id == id);
    res.status(200).send(result);
})

app.get('/users/:usersId', (req, res) => {
    const usersId = Number(req.params.usersId);
    const result = tempArr.filter(el => el.usersId === usersId);
    res.status(200).send(result);
});

app.post('/create', (req, res) => {
    const { usersId, taskName, details } = req.body;
    const data = {
        usersId,
        taskName,
        details,
        id: i,
    };
    tempArr.push(data);
    i++;
    res.status(201).send(data);
});

app.put('/update/:id', (req, res) => {
    const id = req.params.id;
    const newData = req.body;
    const idx = tempArr.findIndex(el => el.id == id);
    if (idx !== -1) {
        tempArr[idx] = {...tempArr[idx], ...newData};
        res.status(200).send({
            msg: "Updated data successfully", 
            data: tempArr[idx]
        });
    } else {
        res.status(404).send({
            msg: "Data not found",
        });
    }
});

app.delete('/delete/:id', (req, res) => {
    const id = req.params.id;
    const idx = tempArr.findIndex(el => el.id == id);
    if (idx !== -1) {
        tempArr.splice(idx, 1);
        res.status(200).send({
            msg: "Deleted data successfully"
        });
    } else {
        res.status(404).send({
            msg: "Data not found",
        });
    }
});

app.listen(port, () => {
    console.log(`Servier is running on port ${port}`);
});
