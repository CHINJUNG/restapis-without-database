#### Remark
Install project

1. Install packages
```$ npm install```

2. Run service api
```$ npm run dev```

** GET / **
- curl --location 'localhost:3000'

** GET /:id **
- curl --location 'localhost:3000/2'

** GET /users/:usersId **
- curl --location 'localhost:3000/usersId/2'

** POST /create **
- curl --location 'localhost:3000/create' \
--header 'Content-Type: application/json' \
--data '{
    "usersId": 1,
    "taskName": "Hello World 1",
    "details": "Test_1"
}'

** PUT /update/:id **
- curl --location --request PUT 'localhost:3000/update/1' \
--header 'Content-Type: application/json' \
--data '{
    "taskName": "Hello World 2",
    "details": "Test_2"
}'

** DELETE /delete/:id **
- curl --location --request DELETE 'localhost:3000/delete/1'
